

var sharedVar = 0

val SYNC = Any()

fun main() {
    var time = System.currentTimeMillis()
    val thread1 = Thread(object: Runnable {
        override fun run() {
            println("${Thread.currentThread().name} started")
            Thread.sleep(100)
            synchronized (SYNC) {
                sharedVar++
                println("${Thread.currentThread().name}. sharedVar = $sharedVar")
            }
        }
    })

    val thread2 = Thread(object: Runnable {
        override fun run() {
            println("${Thread.currentThread().name} started")
            Thread.sleep(50)
            synchronized (SYNC) {
                sharedVar++
                println("${Thread.currentThread().name}. sharedVar = $sharedVar")
            }
        }
    })

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()

    time = System.currentTimeMillis() - time
    println("time = $time")
}