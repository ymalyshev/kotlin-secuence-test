

fun main() {
    val list = listOf(1, 2, 3, 4, 2, 5)
    method1(list)
//    method2(list)
//    method3(list)
}

fun method1(list: List<Int>) {
    val newList = ArrayList(list)
    newList.forEachWithIterator { iterator, i ->
        val shouldRemove = i == 2
        if (shouldRemove) {
            iterator.remove()
            doSomethingRemovedItem(i)
        }
    }
    doSomethingWithNewList(newList)
}

fun method2(list: List<Int>) {
    val itemsToRemove = list.filter { it == 2 }
    itemsToRemove.forEach { doSomethingRemovedItem(it) }
    val newList = ArrayList(list)
    newList.removeAll(itemsToRemove)
    doSomethingWithNewList(newList)
}

fun method3(list: List<Int>) {
    val newList = ArrayList(list)
    newList.asSequence().filter {
        it == 2
    }.forEach {
        newList.remove(it)
        doSomethingRemovedItem(it)
    }
    doSomethingWithNewList(newList)
}

inline fun <T> MutableIterable<T>.forEachWithIterator(block: (MutableIterator<T>, T) -> Unit) {
    val iterator = this.iterator()
    while (iterator.hasNext()) {
        block(iterator, iterator.next())
    }
}

fun doSomethingRemovedItem(data: Any) {
    println("do something with removed item \"$data\"")
}

fun doSomethingWithNewList(newList: List<*>) {
    println("do something with new list: $newList")
}