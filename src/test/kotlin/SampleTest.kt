import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.isNull
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SampleTest {

    @Mock
    private lateinit var testInterface: TestInterface

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testSomething() {
        doSomething(testInterface)
        verify(testInterface, never()).test(any())
    }
}